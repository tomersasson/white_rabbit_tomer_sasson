<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $letterArray = array();
        $fileContents = explode("\n", file_get_contents($filePath));    // read file contents and split to lines
        foreach($fileContents as $line) {
            $letters = str_split(preg_replace("/[^a-zA-Z]+/", "", $line));  // remove all characters but letter
            foreach($letters as $letter) {
                if(empty($letter))
                    continue;
                $lowerCaseLetter = strtolower($letter);
                if(isset($letterArray[$lowerCaseLetter])) {
                    $letterArray[$lowerCaseLetter]++;   // if lower case letter exists, then increase value
                }
                else {
                    $letterArray[$lowerCaseLetter] = 1; // otherwise set to 1
                }
            }
        }

        print_r($letterArray);

        return $letterArray;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        Arsort($parsedFile); // sort the letters bottom up
        $keys = array_keys($parsedFile); // get keys

        $medianIndex = 0;
        if(count($keys) % 2 == 0) { // if n is even
            $medianIndex = count($keys) / 2;
        }
        else { // if odd (for example, if a letter is missing in text)
            $medianIndex = (count($keys) + 1) /2 - 1;
        }

        $medianLetter = $keys[$medianIndex];
        $occurrences = $parsedFile[$medianLetter];
        
        if($medianLetter == "m" && $occurrences < 10000) { // # exception 1
            $medianLetter = $keys[$medianIndex + 1];
            $occurrences = $parsedFile[$medianLetter];
        }
        else if ($medianLetter == "f" && $occurrences > 10000) { // #exception 2
            $medianLetter = $keys[count($keys) - 2];
            $occurrences = $parsedFile[$medianLetter];
        }
        
        print_r($parsedFile);
        echo "Median Letter => ". $medianLetter . "  Occurrences ==> " . $occurrences . "  Total Keys ==> " . count($keys);

        return $medianLetter;
    }
}