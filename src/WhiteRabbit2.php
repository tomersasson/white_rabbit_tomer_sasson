<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $coinInventory = array(100,50,20,10,5,2,1);
        $selectedCoins = array();

        foreach($coinInventory as $coin) {
            $coinCoefficient = intdiv($amount,$coin);
            $remainder = $amount % $coin;
            $selectedCoins[(string)$coin] = $coinCoefficient;
            $amount = $remainder;
        }

        print_r($selectedCoins);

        return $selectedCoins;
    }
}